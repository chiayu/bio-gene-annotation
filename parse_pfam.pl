#!/usr/bin/perl
##Author: Chia-Yu, Hsu
##Date: 2014-09-29
##Aim: transfer Pfam result to format fullfii 4.get_annotations.pl (pfamscan_results.txt)
##Usage:
## cat  /work3/u00cyh00/gene_annotation_genemarkS_new/HM1/pfamscan_results.txt|./parse_pfam.pl>/work3/u00cyh00/gene_annotation_genemarkS_new/HM1/pfamscan_results.txt
use strict;


my $my_hash;

while(my $line = <>){
    chomp($line);
    if($line =~ /^[^#]/){
      my @arr = split(/\s+/, $line);
      ##Pfam result:
      ## <seq id> <alignment start> <alignment end> <envelope start> <envelope end> <hmm acc> <hmm name> <type> <hmm start> <hmm end> <hmm length> <bit score> <E-value> <significance> <clan>
      if(exists($my_hash->{$arr[0]})){
        $my_hash->{$arr[0]} = $my_hash->{$arr[0]} + 1;
      }else{
        $my_hash->{$arr[0]} = 1;
      }

      print "\t";
      print $arr[0]."\t";
      print "\t";
      print $arr[10]."\t"; # length of HMM length
      print $my_hash->{$arr[0]}."\t";
      print $arr[5]."\t";
      print $arr[11]."\t";
      print $arr[12]."\t";
      print $arr[1]."\t";
      print $arr[2]."\n";
    
    }

}
