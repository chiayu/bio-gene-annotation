##  Annotation workflow

##  Strategy

Automatic do BLAST in different BLAST-DB

Fetch data from Uniprot and generate GFF and results

###   System Required:
LSF system
###  Program Required:
BioPerl 1.6.1 +

AB-BLAST

Mysql server

PfamScan(Optional)

### Preparation:
The BLAST-DB you want to do BLAST(required format-db)

(e.g. uniprot_sprot)

###  Step 0. Set MyConfig.pm

###  Step 1. Submit blast to LSF system
    perl 1.submit_blast.pl -prefix HM1 -input /work3/u00cyh00/gene_annotation_genemarkS_new/HM1_aa.fa

###  Step 2. Check the results
    perl 2.check_results.pl -prefix HM1

###  Step 3 Pass the results to a file name blastp2_$DBNAME.out in working dir
    perl 3.parse_results.pl -prefix HM1


###  Step 4.01 Pre- processing the pfamscan result(Optional)
    cat /result_of_pfam.txt|./parse_pfam.pl>pfamscan_results.txt.new

###  Step 4 Paser the results to file
    perl 4.get_annotations.pl -prefix HM1 -file /work3/u00cyh00/gene_annotation_genemarkS_new/HM1.list -pfam pfamscan_results.txt -gtf /work3/u00cyh00/gene_annotation_genemarkS_new/HM1.gff -species HM1 


### In this example, it will create Mysql DB:

####    uniprot_data_2014_07
    +--------------------------------+
    | Tables_in_uniprot_data_2014_07 |
    +--------------------------------+
    | uniref90                       | 
    +--------------------------------+

When running 4.get_annotations.pl, it will fetch data from UniProt Web and insert the data to this DB.
This database and table is created in advance.

####    gene_annotation_new_HM1
    +-----------------------------------+
    | Tables_in_gene_annotation_new_HM1 |
    +-----------------------------------+
    | blastp2_uniref90                  | 
    | gene_info_HM1                     | 
    | pfamscan_results                  | 
    +-----------------------------------+

When running 4.get_annotations.pl, it will create this database and tables, then import data.


