package GetUniprot;
use strict;
use Bio::EnsEMBL::Utils::Argument qw(rearrange);
use DBI;
use MyConfig;
use Bio::SeqIO;
use LWP;
use XML::Simple;
use Data::Dumper;


sub new {
        my($class,@args) = @_;
        my $self = bless {},$class;

        my ($table) = rearrange([qw(TABLE)], @args);

	my $dsn = "DBI:mysql:$UNIPROT_DB;host=$DB_HOST;port=3306";
	my $dbh = DBI->connect($dsn, $DB_USER, $DB_PASS);

	$self->{'_dbh'} = $dbh;
	$self->{'_table'} = $table;

	return $self;
}


sub get_uniref_data {
	my $self = shift;
	my $uniref_id = shift;

	my $dbh = $self->{'_dbh'};
	my $table = $self->{'_table'};
	my $sql = "SELECT * FROM `$table` WHERE `acc` = '$uniref_id'";
	my $sth = $dbh->prepare($sql);
	my $res = $sth->execute();
	
	my $return_string;
	# record exists
	if ($res > 0) {
		my @row = $sth->fetchrow_array();
		$return_string = $row[2]. "\t". $row[0]. "\t". $row[1]. "\t".
		$row[3]. "\t". $row[5]. "\t". $row[4]. "\t". $row[6]. "\t". $row[7];
	}else{
		$return_string = &fetch_data($uniref_id, $self);
	}

	return $return_string;
}

sub fetch_data {
	my $uniref_id = shift;
	my $self = shift;

	my $URL = "http://www.uniprot.org/uniref/$uniref_id.xml";
        my $db;
        my $UserAgent = LWP::UserAgent->new;
        my $req = HTTP::Request->new( GET => $URL);
        my $res = $UserAgent->request( $req );
	my $return_string;

        my $ref = XML::Simple->new()->XMLin( $res->content );

        my $type = $ref->{'entry'}->{'representativeMember'}->{'dbReference'}->{'type'};
        my $id = $ref->{'entry'}->{'representativeMember'}->{'dbReference'}->{'id'};
        if($type eq "UniParc ID"){
                my @properties = @{$ref->{'entry'}->{'representativeMember'}->{'dbReference'}->{'property'}};
                my $species_string;
                my $desc;
                foreach my $property(@properties){
                        my $type = $property->{'type'};
                        if($type eq 'source organism'){
                                $species_string = $property->{'value'};
                        }elsif($type eq 'protein name'){
                                $desc = $property->{'value'}. " [UniParc ID: $id]";
                        }
                }
		my $table = $self->{'_table'};
		my $sql = "INSERT INTO `$table` (`acc`, `organism`, `gene_name`, `description`, `gene_id`, `synonyms`, `kegg`, `go`)".
                        "VALUES (\"$uniref_id\", \"$species_string\", \"\", \"$desc\", \"\", \"\", \"\", \"\")";
                &store_data($sql, $self);
                
		$return_string = "\t". $uniref_id. "\t".$species_string. "\t". $desc. "\t\t\t\t";

        }else{
                $db = 'uniprot';
                my $request = HTTP::Request->new( GET => "http://www.uniprot.org/$db/$id.txt" );
                my $result = $UserAgent->request($request);
                my $string = $result->content;

                my $stringfh;
                open($stringfh, "<", \$string) or die "Could not open string for reading: $!";

                $return_string = &get_wented_feature($stringfh, $uniref_id, $self);
                #print $return_string. "\n"
        }

        return $return_string;
}

sub get_wented_feature {
	my $filehandle = shift;
	my $uniref_id = shift;
	my $self = shift;

	my $io = Bio::SeqIO->new(-fh => $filehandle, -format => "swiss" );
	while(my $seq_obj = $io->next_seq){

		# species
		my $species_object = $seq_obj->species;
		my $species_string = "unidentified";
		if($seq_obj->species){
			$species_string = $species_object->node_name;
		}

		# annotation
		my $anno_collection = $seq_obj->annotation;

		# other features
		my $gene_name = '';
		my @synonyms = ();
		my @go = ();
		my @kegg = ();
		my @gene = ();
		for my $key ( $anno_collection->get_all_annotation_keys ) {
			my @annotations = $anno_collection->get_Annotations($key);
			for my $value ( @annotations ) {
				if($value->tagname eq 'gene_name'){
					if($value->display_text =~ /Name\:\s(\S+)/){
						$gene_name = $1;
					}
					@synonyms = ($value->display_text =~ m/Synonyms\:\s(\S+)/g);
				}elsif($value->tagname eq 'dblink'){
					if($value->display_text =~ /GO\:(\S+)/){
						push(@go, $1);
					}elsif($value->display_text =~ /KEGG\:(\S+)/){
						push(@kegg, $1);
					}elsif($value->display_text =~ /GeneID\:(\S+)/){
						push(@gene, $1);
					}
				}
			}
		}



		# description
		my $desc_object = $seq_obj->desc;


		my $synonyms_string = join(', ', @synonyms);
		my $id_string = join(', ', @gene);
		my $kegg_string = join(', ', @kegg);
		my $go_string = join(', ', @go);

		# store data
		my $table = $self->{'_table'};
		my $sql = "INSERT INTO `$table` (`acc`, `organism`, `gene_name`, `description`, `gene_id`, `synonyms`, `kegg`, `go`)".
			"VALUES (\"$uniref_id\", \"$species_string\", \"$gene_name\", \"$desc_object\", \"$id_string\", \"$synonyms_string\", \"$kegg_string\", \"$go_string\")";
		&store_data($sql, $self);
		
		my $return_string = $gene_name. "\t". $uniref_id. "\t". $species_string. "\t".
			$desc_object. "\t". $synonyms_string. "\t". $id_string. "\t".
			$kegg_string. "\t". $go_string;

		return $return_string; 
	}
}

sub store_data{
	my $sql = shift;
	my $self = shift;

	my $dbh = $self->{'_dbh'};
	my $sth = $dbh->prepare($sql);
	$sth->execute();
}

1;
