#!/usr/bin/perl

use strict;

use Bio::SearchIO;

# Prompt the user for the file name if it's not an argument
# NOTE: BLAST file must be in text (not html) format
my $inFile;
if (! $ARGV[0])
{
   print "What is the BLAST file to parse? ";

   # Get input and remove the newline character at the end
   chomp ($inFile = <STDIN>);
}
else
{
   $inFile = $ARGV[0];
}

my $report = new Bio::SearchIO(
         -file=>"$inFile",
              -format => "blast"); 

#print "QueryAcc\tQueryLen\tHitAccess\tHitDesc\tHitLength\tNumHsps\tHspRank\tHspLength\tQueryStart\tQueryEnd\tHitStart\tHitEnd\tHitStrand\t\%ID\teValue\n";
#print "QueryAcc\tQueryLen\tHitAccess\tHitDesc\tHitLength\tNumHsps\tHspRank\tHspLength\tQueryStart\tQueryEnd\tHitStart\tHitEnd\t\%ID\teValue\n"; # without strand
#print "QueryAcc\tQueryLen\tHitAccess\tHitDesc\tHitLength\tNumHsps\tHspRank\tHspLength\tQueryStart\tQueryEnd\tHitStart\tHitEnd\t\%ID\tcoverage\teValue\n"; # with coverage

my $id;
my $flag = 0;
# Go through BLAST reports one by one              
while(my $result = $report->next_result) 
{
   # Go through each each matching sequence
   while(my $hit = $result->next_hit) 
   { 
      # Go through each each HSP for this sequence
        while (my $hsp = $hit->next_hsp)
         { 
   if($result->query_accession eq $id){
	$flag++;
   }else{
	$id = $result->query_accession;
	$flag = 1;
   }
            # Print some tab-delimited data about this HSP
            print "\t";
            print $result->query_accession, "\t";
            #print $result->query_name, "\t";
	    print $result->query_length, "\t";
			if($hit->name =~ /\|/){
				my @a = split(/\|/, $hit->name);
				print $a[2]. "\t";
			}else{
				print $hit->name, "\t";
			}
            print $hit->description, "\t";
	    print $hit->length, "\t";
	    print $hit->num_hsps, "\t";
            print $hsp->rank, "\t";
            print $hsp->hsp_length, "\t"; #include gaps
	    print $hsp->start('query'), "\t";
	    print $hsp->end('query'), "\t";
	    print $hsp->start('hit'), "\t";
	    print $hsp->end('hit'), "\t";
	    #print $hsp->strand('query'), "\t";
            print $hsp->percent_identity, "\t";
	    my $len = $hsp->end('query') - $hsp->start('query') + 1;
	    print $len / $result->query_length, "\t";
            #print $hit->significance, "\t";
	    my $e = $hsp->evalue;
	    $e =~ s/\,//g;
            print $e, "\t";
	    print $flag. "\n";
      } 
   } 
}
