#!/usr/bin/perl
##Author: Chia-Yu, Hsu
##Date: 2014-10-05
##Aim: Get the average time of jobs
##Usage:
##      perl 5.get_statistics.pl --prefix pc_uniref

use strict;
use Getopt::Long;
use MyConfig;
use List::Util qw/max min/;

my $prefix;
GetOptions (
	"prefix=s"  => \$prefix
);

unless($prefix){
	die "Usage: perl 5.get_statistics.pl  -prefix 'the prefix you specified when running blast'\n";
}

my $work_dir = $WORK_DIR;
my $path = $work_dir. "/". $prefix; 
my @dirs = glob("$path/*");
#my @dirs = glob("$work_dir/*");
foreach my $this_dir(@dirs){
    mkdir($prefix);
    chdir("./$prefix");
	&get_stat($this_dir);
    chdir("../");
}

sub get_stat {
	my $dir = shift;

    my @arr_start_time;
    my @arr_end_time;
    my @arr_running_hours;
    my @arr_max_swap;
    my @arr_max_memory;
    my $total_cpu_time;
    
    open(TMP, ">>temp.stat");

	my @files = glob("$dir/*.out");
	foreach my $file(@files){
        print "\t".$file."\n";
		
        open(IN, $file);

        my $start_time;
        my $end_time;

		while(my $line = <IN>){
			chomp($line);
			if($line =~ s/^Started at //g ){
                $start_time = `echo \$(date +%s -d '$line')`;
                push(@arr_start_time, $start_time);
            }
			if($line =~ s/^Results reported at //g ){
                $end_time = `echo \$(date +%s -d '$line')`;
                push(@arr_end_time, $end_time);
            }

            if($line =~ /\s+CPU time\s+:\s+(\d+)/){
                $total_cpu_time = $total_cpu_time + $1;
            }
            if($line =~ /^\s+Max Memory\s+:\s+(\d+)/ ){
                push(@arr_max_memory, $1);
            }
            if($line =~ /^\s+Max Swap\s+:\s+(\d+)/ ){
                push(@arr_max_swap, $1);
            }
		}
        
        my $command = $end_time - $start_time;
        my $hours =  ($command / 3600);
        push(@arr_running_hours, $hours);
        
        print TMP $hours."\n";
		
        close($file);
       
	}
    close(TMP);

    my $running_time = (max(@arr_end_time)-  min(@arr_start_time)) /86400  ;#DAY;
    &print_to_file( $dir, $running_time, $total_cpu_time, max(@arr_max_swap), max(@arr_max_memory));    
    
   
    system('R<../draw.r --vanilla --slave');
}


sub print_html{



}


sub print_to_file{
    my ($dir, $total_running_time, $total_cpu_time, $max_swap, $max_memory) =  @_;

	my @files = glob("$dir/*.fa");
    my $fasta_file = shift @files;
    my $sequence_per_fasta = `grep '>' $fasta_file|wc -l`;
    my $job_num = scalar(@files);

    my $current_dir = &get_current_dir($dir);
    my $out_file_name = $current_dir.".stat";
    open(OUT, ">>$out_file_name");


    print OUT "=============================================="."\n";
    print OUT "      $current_dir"."\n";
    print OUT "=============================================="."\n";
    print OUT "Number of jobs: ".$job_num."\n";
    print OUT "Number of sequences per job: ".$sequence_per_fasta."\n";
    print OUT "Max Swap: ".$max_swap." GB \n";
    print OUT "Max memory: ".$max_memory." GB\n";
    print OUT "Total CPU time: ".$total_cpu_time." secs \n";
    print OUT "Total runing time: ".$total_running_time." days \n";

    close(OUT);
}
sub get_current_dir{
    my $abs_dir = shift @_;
    my @dirs = split("\/", $abs_dir);
    my $current_dir;

    while(@dirs){
        $current_dir = shift @dirs;
    }

    return $current_dir;
}
