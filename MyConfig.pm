package MyConfig;
use strict;

use vars qw( %MyConfig );

%MyConfig = (
	DBS      => ['uniref90', 
                      ],
	#DBS      => ['uniprot_sprot', 'uniprot_trembl_fungi', 'uniprot_sprot_fungi'],
	#DBS      => ['uniprot_trembl', 'uniprot_sprot'],
	ORDER    => [#'uniprot_sprot_bacteria', 
                     #'uniprot_sprot_viruses', 
                     #'uniprot_trembl_bacteria', 
                     #'uniprot_trembl_viruses', 
                     'uniref90',
                     ], # the order is very important, determines the anootation process
	#ORDER    => ['uniprot_sprot', 'uniprot_trembl'], # the order is very important, determines the anootation process
	WORK_DIR => '/work3/u00cyh00/gene_annotation_hp/blast_results',
	#WORK_DIR => '/home/u00cyh00/gene_an`notation/blast_results',
	DB_PATH  => '/work3/u00cyh00/gene_annotation_hp/data_from_uniRef90',
	#DB_PATH  => '/work3/u00cyh00/uniprot/2013_07',
	WORKER_NUM => 50,
	NORMAL_QUEUE => '4G',
	LARGE_QUEUE => '16G',

	DB_HOST  => '10.3.9.100',
	DB_USER  => 'u00cyh00',
	DB_PASS  => '',
	UNIPROT_DB => 'uniprot_data_2014_07',

	COVERAGE => 0.40,
);

sub import {
        my ($callpack) = caller(0); # Name of the calling package
        my $pack = shift; # Need to move package off @_

	# Get list of variables supplied, or else
	# all of GeneConf:
        my @vars = @_ ? @_ : keys( %MyConfig );
        return unless @vars;

	# Predeclare global variables in calling package
        eval "package $callpack; use vars qw("
                . join(' ', map { '$'.$_ } @vars) . ")";
        die $@ if $@;


        foreach (@vars) {
                if ( defined $MyConfig{ $_ } ) {
                        no strict 'refs';
			# Exporter does a similar job to the following
			# statement, but for function names, not
			# scalar variables:
                        *{"${callpack}::$_"} = \$MyConfig{ $_ };
                } else {
                        die "Error: MyConfig: $_ not known\n";
                }
        }
}

 
1;
