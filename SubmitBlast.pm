package SubmitBlast;
use strict;
use Bio::EnsEMBL::Utils::Argument qw(rearrange);
use Bio::SeqIO;
use File::Path qw(mkpath);

sub new {
	my($class,@args) = @_;
	my $self = bless {},$class;

	my ($input, $prefix, $blast_db, $work_dir, $blastp, $worker, $queue) =
		rearrange([qw(INPUT PREFIX DB WORK_DIR BLASTP WORKER QUEUE)], @args);

	$blast_db =~ /(uniprot_\w+)\./;
	$work_dir .= "/$prefix/$1";
	$self->{_input}     = $input;
	$self->{_prefix}    = $prefix;
	$self->{_db}        = $blast_db;
	$self->{_blastp}    = $blastp || '/home/u00cyh00/ensembl_pipeline/prg/ab_blast/blastp';
	$self->{_worker}    = $worker || 100;
	$self->{_work_dir}  = $work_dir;
	$self->{_queue}     = $queue || '16G';
 	return $self;
}

sub check_options {
	my ($self,@args) = @_;

	unless(defined($self->{_input}) && defined($self->{_db}) && defined($self->{_prefix})){
		warn "Please define the following options:
				-input
				-db
				-prefix\n";
		return 0;
	}else{
		return 1;
	}
}

sub check_dir {
	my ($self,@args) = @_;

	my $work_dir = $self->{_work_dir};
	if(-d $work_dir){
		die "Failed: $work_dir exists\n";
	}else{
		eval(mkpath($work_dir));
		if ($@){ 
			die "Couldn't create $work_dir: $@\n"; 
		}else{ 
			print "mkdir good!\n";
		}
	}
	return $self;
}

sub split_input {
	my ($self, @args) = @_;

	my $in = Bio::SeqIO->new(-file => $self->{_input} ,
        		  -format => 'Fasta');
	my @seqs = ();
	my $count = 0;
	while(my $s = $in->next_seq()){
		push(@seqs, $s);
		$count++;
	}

	my $num = int($count / $self->{_worker});
	my $n = int($count / $num);
	if($count % $num){
		$n++;
	}else{;}

	my $i = 1;
	my $f = 1;
	my $out;
	foreach my $seq(@seqs){
		if($i % $num eq 1){
			$out = Bio::SeqIO->new(-file => ">$self->{_work_dir}/blast_input_$f.fa" ,
					-format => 'Fasta');
			$f++;
		}
		$out->write_seq($seq);
		$i++;
	}
	return $self;
}

sub run {
	my ($self, @args) = @_;

	my @blasts = glob("$self->{_work_dir}/blast_input_*.fa");
	foreach my $blast(@blasts){
		print $blast. "...............\n";
		my $sh = $blast;
		$sh =~ s/\.fa$/.sh/;
		$blast =~ /(blast_input_\d+)/;
		my $name = $1;
		my $err = $blast;
		$err =~ s/\.fa$/.err/;
		my $out = $blast;
		$out =~ s/\.fa$/.out/;

		open(SCRIPT, ">$sh") or die $!;
		print SCRIPT "#!/bin/sh\n";
		print SCRIPT "#BSUB -q $self->{_queue}\n";
		#print SCRIPT "#BSUB -n 1\n";
		print SCRIPT "#BSUB -R 'span[hosts=1]'\n";
		print SCRIPT "#BSUB -o $out\n";
		print SCRIPT "#BSUB -e $err\n";
		print SCRIPT "#BSUB -J 'gene_annotation'\n";
		#print SCRIPT "#BSUB -R 'span[hosts=1]'\n";
		if($self->{_queue} eq "16G"){
			print SCRIPT "$self->{_blastp} $self->{_db} $blast -cpus 6 -wordmask seg -E 1e-4 -v 20 -b 10\n";
		}elsif($self->{_queue} eq "48G"){
			print SCRIPT "$self->{_blastp} $self->{_db} $blast -cpus 20 -wordmask seg -E 1e-4 -v 20 -b 10\n";
		}elsif($self->{_queue} eq "4G"){
			print SCRIPT "$self->{_blastp} $self->{_db} $blast -wordmask seg -E 1e-4 -v 20 -b 10\n";
		}else{}
		close(SCRIPT);
	
		chmod(0755, $sh) or die $!;

		system("bsub < $sh") == 0 or die "Can't submit job: $sh!!\n";
	}
	return $self;
}
1;
