#!/usr/bin/perl
use strict;
use MyConfig;
use Getopt::Long;
use Bio::SearchIO;

my $prefix;
GetOptions (
	"prefix=s"  =>  \$prefix,
);

unless($prefix){
	die "Usage:
	perl parse_results.pl -prefix test\n";
}

my $work_dir = $WORK_DIR;
my $path = $work_dir. "/". $prefix;

#my @dirs = glob("$path/*");
foreach my $this_db(@$DBS){
	#if($this_db =~ /trembl/){
	#	next;
	#}
	#$this_db =~ s/uniprot_//;
	my $dir = $path."/".$this_db;
	my $out_file = $path."/blastp2_".$this_db.".out";
	&do_parse($dir, $out_file);
}

sub do_parse {
	my $path = shift;
	my $out_file = shift;

	my @files = glob("$path/*.out");
	open (my $out_handle, ">$out_file") or die "$!";
	foreach my $file(@files){
		&parse_blast($file, $out_handle);
	}
	close($out_handle);
}

sub parse_blast {
	my $inFile = shift;
	my $out = shift;

	my $report = new Bio::SearchIO(
			-file=>"$inFile",
			-format => "blast"); 
	my $id;
	my $flag = 0;
	# Go through BLAST reports one by one              
	while(my $result = $report->next_result) 
	{
		# Go through each each matching sequence
		while(my $hit = $result->next_hit) 
		{ 
			# Go through each each HSP for this sequence
			while (my $hsp = $hit->next_hsp)
			{ 
				if($result->query_accession == $id){
					$flag++;
				}else{
					$id = $result->query_accession;
					$flag = 1;
				}
				# Print some tab-delimited data about this HSP
				print $out "\t";
				print $out $result->query_accession, "\t";
				#print $out $result->query_name, "\t";
				print $out $result->query_length, "\t";
				if($hit->name =~ /\|/){
					my @a = split(/\|/, $hit->name);
					print $out $a[2]. "\t";
				}else{
					print $out $hit->name, "\t";
				}
				print $out $hit->description, "\t";
				print $out $hit->length, "\t";
				print $out $hit->num_hsps, "\t";
				print $out $hsp->rank, "\t";
				print $out $hsp->hsp_length, "\t"; #include gaps
				print $out $hsp->start('query'), "\t";
				print $out $hsp->end('query'), "\t";
				print $out $hsp->start('hit'), "\t";
				print $out $hsp->end('hit'), "\t";
				#print $out $hsp->strand('query'), "\t";
				print $out $hsp->percent_identity, "\t";
				my $len = $hsp->end('query') - $hsp->start('query') + 1;
				print $out $len / $result->query_length, "\t";
				#print $out $hit->significance, "\t";
				my $e = $hsp->evalue;
				$e =~ s/\,//g;
				print $out $e, "\t";
				print $out $flag. "\n";
			} 
		} 
	}
}
