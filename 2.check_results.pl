# Check is there any error occured on the blast processes.
# perl check_results.pl -prefix test
#!/usr/bin/perl
use strict;
use Getopt::Long;
use MyConfig;

my $prefix;
GetOptions (
	"prefix=s"  => \$prefix
);

unless($prefix){
	die "Usage: perl check_results.pl -prefix 'the prefix you specified when running blast'\n";
}
my $work_dir = $WORK_DIR;
my $path = $work_dir. "/". $prefix; 

my @failed;
my @dirs = glob("$path/*");
foreach my $this_dir(@dirs){
	&check_error($this_dir);
}

sub check_error {
	my $dir = shift;

	print $dir. "\n";

	my @files = glob("$dir/*.out");
	foreach my $file(@files){
                open(IN, $file);
		my $fail = 1;
                while(my $line = <IN>){
                        chomp($line);
			if($line =~ /Successfully completed\./){
				$fail = 0;		
			}
		}
		push(@failed, $file) if($fail);
		close(IN);
	}
}

print "Failed batchs:\n";
foreach my $fail_file(@failed){
	print $fail_file. "\n";
	# re-submit jobs
}
