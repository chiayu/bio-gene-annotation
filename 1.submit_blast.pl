#!/usr/bin/perl
use strict;
use SubmitBlast;
use Getopt::Long;
use MyConfig;

my $input;
my $prefix;
#my $db_path = "/work3/u00wly00/uniprot/2013_07";
my $db_path = $DB_PATH;

GetOptions (
	"input=s"  =>  \$input,
	"prefix=s" =>  \$prefix,
);

unless($input && $prefix){
	die "Usage:
	perl submit_blast.pl -prefix test -input /path/to/your/protein/seqs\n";
}


foreach my $db(@$DBS){
	
	my $blast;
	if($db eq 'uniprot_trembl_all'){
		$blast = new SubmitBlast(
				-input  => $input,
				-db     => $db_path.'/'.$db.'.fasta',
				-prefix => $prefix,
				-work_dir => $WORK_DIR,
				-worker => $WORKER_NUM,
				-queue  => $LARGE_QUEUE,
				);
	}else{
		$blast = new SubmitBlast(
				-input  => $input,
				-db     => $db_path.'/'.$db.'.fasta',
				-prefix => $prefix,
				-work_dir => $WORK_DIR,
				-worker => $WORKER_NUM,
				-queue  => $NORMAL_QUEUE,
				);
	}

	# check arguments
	unless($blast->check_options){
		die("Check failed!!\n");
	}

	$blast->check_dir;
	$blast->split_input;
	$blast->run;
}
