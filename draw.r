#!/usr/bin/R
png("time.png");
x <- read.delim("./temp.stat", header = FALSE);
plot(x$V1,xlab=c("JOB index, mean= ",mean(x$V1)), ylim=c(0,max(x$V1)), ylab = "Hours",pch = 4 , type = "o" , main= "Distribution of time consuming")
dev.off();

