# Uniref (xml file)
# data fecth from uniprot
#!/usr/bin/perl
use strict;
use MyConfig;
use DBI;
use Getopt::Long;
use GetUniprot;

my $prefix;
my $file;
my $pfam;
my $gtf;
my $spe_prefix;
GetOptions (
	'prefix=s' => \$prefix,
	'pfam=s'   => \$pfam,
	'gtf=s'    => \$gtf,
	'species=s'=> \$spe_prefix  # for gene name prefix
);

unless($prefix && $gtf && $spe_prefix){
	die "Usage:
		perl 4.get_annotations.pl -prefix test2 -gtf ./data/GMUA_130525/genemark_hmm.gtf -species GMUA  \n
        (-pfam ./data/GMUA_130525/pfamscan_results.txt) \n\n
        ------------------------\n
        -pfam (optional) the file name of pfam results must be 'pfamscan_results.txt'\n";
}


# create databse and import blast results
&create_database();

# connect database
my $dsn = "DBI:mysql:gene_annotation_$prefix;host=$DB_HOST;port=3306";
my $dbh = DBI->connect($dsn, $DB_USER, $DB_PASS);
my $dsn_uniprot = "DBI:mysql:$UNIPROT_DB;host=$DB_HOST;port=3306";
my $dbh_uniprot = DBI->connect($dsn_uniprot, $DB_USER, $DB_PASS);

# get annoation from blast results
&get_gene_info($gtf);

# import gene info into database
&create_gene_info_table();

# import pfamscan results into database
&create_pfam_table() if($pfam);

# get pfam results if provided
my $pfam_data; 
$pfam_data = &get_pfam_desc() if($pfam);
open(PFAMOUT, ">pfam_annotated_genes_$prefix.txt") if($pfam);

# get exon info from gtf
my $ref = &get_exon_from_gtf();

# generate gff
&generate_gff($ref);


sub get_blast {
	my $gene = shift;
	my $table = shift;

	my $sql = "SELECT * FROM $table where `query_id` = '$gene' and `rank` = 1 and `coverage` > '$COVERAGE'";
	my $sth = $dbh->prepare($sql);
	$sth -> execute();
	while(my @row = $sth -> fetchrow_array()){
		#print $row[3]. "\n";
		return $row[3], $row[15], $row[14];
	}
}

sub get_gene_info {
    my $file = shift;
    open(IN, $file);
    my $anno_file = "gene_info_$prefix.txt";
    open(OUT, ">$anno_file");
    my $done_list;
    while(my $line = <IN>){
        chomp($line);
    	if($line =~ /^$/ or $line =~ /^\#/){
        	next;
    	}
        my @array = split("\t", $line);
        if($array[8] =~ /gene_id\s\"(\d+\_g)\"/){

        }elsif($array[8] =~ /gene_id\s(\d+)/){

        }elsif($array[8] =~ /gene_id\=(\d+)/){

        }else{
                die("ERROR: Unvalidated gene id in $gtf\n");
        }
        my $gene = $1;
	next if(exists $done_list->{$gene});

        print OUT $gene. "\t";
        my ($result, $evalue, $coverage);
        foreach my $this_db(@$ORDER){
            ($result, $evalue, $coverage) = &get_blast($gene, 'blastp2_'.$this_db);
            if($result){
                my $db_name = $this_db;
                $db_name =~ s/^(uniprot\_sprot)$/$1_all/;
                $db_name =~ s/^(uniprot\_trembl)$/$1_all/;
                #print OUT &get_anno_from_blast($result, $db_name."_data");
                print OUT &get_anno_from_uniprot($result, $this_db)."\t";
                print OUT $array[0]. "\t";
                print OUT $coverage. "\t";
                print OUT $evalue. "\t";
                print OUT $db_name. "\n";
                last;
            }
        }
        # not hit
        unless($result){
            print OUT "\t\t\t\t\t\t\t\t\t\t\t\tundefined\n";
        }

	$done_list->{$gene} = $gene;
    }   
    close(IN);
    close(OUT);
}

sub get_anno_from_uniprot {
	my $id = shift;
	my $table = shift;

	my $uni = new GetUniprot(-table=>$table);
	my $string = $uni->get_uniref_data($id);

	return $string;
}

sub get_anno_from_blast {
        my $id = shift;
	my $table = shift;	

        my $sql = "SELECT * FROM $table WHERE `acc` = '$id'";
        my $sth = $dbh_uniprot->prepare($sql);
        $sth -> execute();
        my $name;
        my $string;
        while(my @row = $sth -> fetchrow_array()){
                #if($row[2] ne ''){
                        $name = $row[2];
                #}else{
                #       $name = $row[0];
                #}
                my $syn_check;
                #if ($name && $row[1] eq "Saccharomyces cerevisiae"){
                #       $syn_check = &check_synonyms($name, $row[5]); # add alias from sgd info.
                        #print $syn_check. "\n";
                #}
                $string = $name. "\t". $row[0]. "\t". $row[1]. "\t". $row[3]. "\t". $syn_check. "\t". $row[4]. "\t". $row[6]. "\t". $row[7]. "\t";
        }
        #if($string eq ''){
        #       $string = "\t\t\t\t\t\t\t\t";
        #}
        return $string;
}

sub get_exon_from_gtf {
    open(IN, $gtf);
    my $ref;
    while(my $line = <IN>){
            chomp($line);
    	    if($line =~ /^$/ or $line =~ /^\#/){
        	next;
    	    }
            my @array = split("\t", $line);
            if($array[8] =~ /gene_id\s\"(\d+\_g)\"/){
	    
	    }elsif($array[8] =~ /gene_id\s(\d+)/){

	    }elsif($array[8] =~ /gene_id\=(\d+)/){

	    }else{
		die("ERROR: Unvalidated gene id in $gtf\n");
	    }
            my $gene_id = $1;
	    $gene_id .= '_g' unless $gene_id =~ /\_g/;
            my $chr = $array[0];
            my $type = $array[2];
            my $strand = $array[6];
            my $start;
            my $end;
            $ref->{$array[0]}->{$gene_id}->{"strand"} = $strand;
            if($type eq "start_codon"){
                    if($strand eq "+"){
                            $start = $array[3];
                    }elsif($strand eq "-"){
                            $start = $array[4];
                    }
                    $ref->{$array[0]}->{$gene_id}->{"start"} = $start;
            }elsif($type eq "stop_codon"){
                    if($strand eq "+"){
                            $end = $array[4];
                    }elsif($strand eq "-"){
                            $end = $array[3];
                    }
                    $ref->{$array[0]}->{$gene_id}->{"end"} = $end;
            }elsif($type eq "CDS"){
                    my $range = $array[3]. "-". $array[4];
                    push (@{$ref->{$array[0]}->{$gene_id}->{"CDS"}}, $range);
            }
    }
    close(IN);
    return $ref;
}

sub generate_gff {
    my $ref = shift;
    open(FINALOUT, ">$prefix.gff");
    foreach my $chr(keys %$ref){
            #print $chr. "\n";
            # gene
            foreach my $gene(keys %{$ref->{$chr}}){
                    #print $gene. "----------->\n";
                    my $symbol = $gene;
                    $symbol =~ s/\_g//;
                    #CDS
                    my $gene_start = 0;
                    my $gene_end = 0;
                    my @cds_strings = ();
                    foreach my $range(@{$ref->{$chr}->{$gene}->{"CDS"}}){
                            my ($start, $end) = split("-", $range);
                            if($end > $gene_end){
                                    $gene_end = $end;
                            }else{;}
                            if($gene_start == 0){
                                    $gene_start = $start;
                            }elsif($start < $gene_start){
                                    $gene_start = $start;
                            }else{;}
                            my $string = "$chr\tENSEMBL\tCDS\t$start\t$end\t.\t$ref->{$chr}->{$gene}->{'strand'}\t.\tlocus_tag=$spe_prefix\_$symbol\n";
                            push(@cds_strings, $string);
                    }
                    my $desc = &get_anno_desc($gene);
                    print FINALOUT $chr. "\tENSEMBL\tgene\t$gene_start\t$gene_end\t.\t$ref->{$chr}->{$gene}->{'strand'}\t.\tlocus_tag=$spe_prefix\_$symbol;product=$desc\n";
                    my $chr_num = $chr;
                    $chr_num =~ s/GMUB-chr//;
                    $chr_num =~ s/^0//;
                    #print $gene. "\t". "chromosome:GLB.1:$chr:$gene_start-$gene_end:$ref->{$chr}->{$gene}->{'strand'}\n"; 
                    print FINALOUT join('', @cds_strings);
            }
    }
    close(FINALOUT);
}
sub get_anno_desc{
        my $gene = shift;
	my $table = "gene_info_$prefix";
        my $sql = "SELECT description, uniprot FROM $table WHERE id = '$gene'";
        my $sth = $dbh->prepare($sql);
        $sth->execute;
        my $desc;
        while(my @row = $sth->fetchrow_array()){
                if($row[0]){
                        $row[0] =~ /Full\=(.*?)\;/;
                        $desc = $1. '(BCT: hit to '.$row[1].')';
                }else{
                        #$desc = 'hypothetical protein';
                        my $pfam_id;
			$pfam_id = &get_pfam($gene) if($pfam);
                        if($pfam_id){
                                $desc = $pfam_data->{$pfam_id}. " containing protein";
                                print PFAMOUT $gene. "\t". $desc. "\n";
                        }else{
                                $desc = 'hypothetical protein (BCT: without any hits to uniprot)';
                        }
                }
        }
        return $desc;
}

sub get_pfam{
        my $gene = shift;
        my $pfam_table = 'pfamscan_results';
        my $sql = "SELECT hit_id FROM $pfam_table WHERE gene_id = '$gene' ORDER BY evalue LIMIT 1";
        my $sth = $dbh->prepare($sql);
    $sth->execute;
        my $pfam;
        while(my @row = $sth->fetchrow_array()){
                $pfam = $row[0];
        }
        return $pfam;
}

sub get_pfam_desc{
        my $file = './data/pfam/Pfam-A-v27.list';

        my $ref;
        open(IN, $file);
        while(my $line = <IN>){
                chomp($line);
                my @array = split("\t", $line);
                #$ref->{$array[1]} = $array[2];
                $ref->{$array[0]} = $array[2];
        }
        return $ref;
}

sub create_database {
# create database;
    system("mysql -u $DB_USER -h $DB_HOST -p$DB_PASS -e 'create database gene_annotation_$prefix' ") == 0 
        or die "Create database failed: $?\n";

    foreach my $this_db(@$DBS){
# table schema
        my $sql = &generate_sql($this_db);
        system("mysql -u $DB_USER -h $DB_HOST -p$DB_PASS gene_annotation_$prefix -e '$sql' ") == 0
            or die "Create table $this_db failed: $?\n";

# import data
        my $data_file = $WORK_DIR.'/'.$prefix.'/blastp2_'.$this_db.'.out';
        system("mysqlimport -u $DB_USER -h $DB_HOST -p$DB_PASS -L -l gene_annotation_$prefix $data_file") == 0
            or die "Import data into table $this_db failed: $?\n";

    }
}

sub generate_sql {
    my $name = shift;

    my $sql = "
        CREATE TABLE `blastp2_$name` (
                `id` int(10) unsigned NOT NULL auto_increment,
                `query_id` varchar(40) NOT NULL,
                `query_len` int(10) default NULL,
                `subject_id` varchar(40) default NULL,
                `subject_desc` text,
                `subject_len` int(20) default NULL,
                `num_hsp` int(10) default NULL,
                `hsp` int(10) default NULL,
                `hit_len` int(20) default NULL,
                `query_start` int(20) default NULL,
                `query_end` int(20) default NULL,
                `sbjct_start` int(20) default NULL,
                `sbjct_end` int(20) default NULL,
                `identity` float default NULL,
                `coverage` float default NULL,
                `evalue` double default NULL,
                `rank` int(10) default NULL,
                PRIMARY KEY  (`id`),
                KEY `query_id` (`query_id`,`subject_id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
    ";

    return $sql;
}
sub create_gene_info_table{

        my $sql = "
                DROP TABLE IF EXISTS `gene_info_$prefix`;
                CREATE TABLE `gene_info_$prefix` (
                                `id` varchar(20) NOT NULL,
                                `name` varchar(40) default NULL,
                                `uniprot` varchar(40) default NULL,
                                `organism` varchar(100) default NULL,
                                `description` varchar(750) default NULL,
                                `synonyms` varchar(100) default NULL,
                                `gene_id` text,
                                `kegg` text,
                                `go` text,
                                `chromosome` int(2) default NULL,
                                `coverage` float default NULL,
                                `evalue` double default NULL,
                                `source` varchar(100) default NULL,
                                `contig` varchar(20) default NULL,
                                `contig_link` varchar(60) default NULL,
                                PRIMARY KEY  (`id`),
                                KEY `name` (`name`,`uniprot`,`synonyms`,`description`,`chromosome`)
                                ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
        ";

        # create table
        system("mysql -u $DB_USER -h $DB_HOST -p$DB_PASS gene_annotation_$prefix -e '$sql' ") == 0
                or die "Create table gene_info_$prefix failed: $?\n";

        # import table
        system("mysqlimport -u $DB_USER -h $DB_HOST -p$DB_PASS -L -l gene_annotation_$prefix gene_info_$prefix.txt") == 0
                        or die "Import data into table gene_info_$prefix failed: $?\n";
}

sub create_pfam_table {
        my $sql = "
                DROP TABLE IF EXISTS `pfamscan_results`;
                CREATE TABLE `pfamscan_results` (
                                `id` int(10) unsigned NOT NULL auto_increment,
                                `gene_id` varchar(20) default NULL,
                                `description` varchar(100) default NULL,
                                `length` int(10) default NULL,
                                `num_hits` int(10) default NULL,
                                `hit_id` varchar(40) default NULL,
                                `score` float default NULL,
                                `evalue` double default NULL,
                                `qstart` int(20) default NULL,
                                `qend` int(20) default NULL,
                                PRIMARY KEY  (`id`),
                                KEY `gene_id` (`gene_id`,`hit_id`)
                                ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
        ";

        # create table
        system("mysql -u $DB_USER -h $DB_HOST -p$DB_PASS gene_annotation_$prefix -e '$sql' ") == 0
                or die "Create table pfamscan_results failed: $?\n";

        # import table
        system("mysqlimport -u $DB_USER -h $DB_HOST -p$DB_PASS -L -l gene_annotation_$prefix $pfam") == 0
                        or die "Import data into table pfamscan_results failed: $?\n";
}
